

$("#sections").change(function(){


    if( $(this).val() == 0 ) {
        alert("یک مقطع انتخاب کنید!");
        return false;
    }

    $("#classes").empty()
    $("#classes").append('<option value="0" selected>-- انتخاب کنید --</option>')
    var url = "/admin/sections/" + $(this).val() + "/class";

    $.ajax({
        url: url,
        success: function(result){

            $.each(result.data, function (i, v) {
               $("#classes").append('<option value="'+ v.id +'" selected>' + v.title + '</option>')
            });

        }});
});
