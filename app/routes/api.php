<?php

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix( 'admin')->namespace('\App\Http\Controllers\Admin')->group(function() {


    Route::get('/dashboard','AdminController@dashboard');

    Route::prefix( 'class-rooms')->namespace('ClassRoom')->group(function() {

        Route::get('/','AdminClassRoomController@allClassRoom')->name('all.class.room');
        Route::get('/create','AdminClassRoomController@createClassRoom');
        Route::post('/store','AdminClassRoomController@storeClassRoom')->name('store.class.room');
        Route::post('/delete/{id}','AdminClassRoomController@delete')->name('delete.class.room');
    });



    Route::prefix( 'students')->namespace('Student')->group(function() {
        Route::post('/store','AdminStudentController@storeStudent')->name('store.student');
        Route::get('/','AdminStudentController@allStudent');
    });


    Route::prefix( 'sections')->namespace('Section')->group(function() {
        Route::get('/{section_id}/class','AdminSectionController@getClassesSection');
    });
});

