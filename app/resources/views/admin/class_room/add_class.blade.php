@extends('admin.master')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    ثبت کلاس جدید
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-6">

                            <form role="form" action="{!! route('store.class.room') !!}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>نام کلاس </label>
                                    <input class="form-control"value="" name="title">
                                    <p class="help-block">لطفا نام کلاس را انتخا کنید</p>
                                </div>

                                <div class="form-group">
                                    <label>مقطع</label>
                                    <select class="form-control" name="section_id">

                                        <option value="0" selected>-- انتخاب کنید --</option>

                                        @foreach($sections as $section)
                                            <option value="{{$section->id}}"> {{$section->title}}</option>
                                        @endforeach


                                    </select>
                                </div>

                                <button type="submit" class="btn btn-default">ایجاد کلاس جدید</button>

                            </form>

                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>




@endsection
