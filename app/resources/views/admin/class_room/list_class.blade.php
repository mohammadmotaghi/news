@extends('admin.master')

@section('content')

    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>نام کلاس</th>
                            <th>مقطع کلاس</th>
                            <th>ویرایش</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($list as $item)
                            <tr>
                                <td>{{$item->title}}</td>

                                <td>{{$item->section->title}}</td>

                                <td>
                                    <form {!! route('delete.class.room',['id' => $item->id ]) !!} method="delete">
                                    <a href="#" class="btn btn-danger btn-block">حذف</a>
                                    </form>
                                </td>

                                <td><a href="#" class="btn btn-warning btn-block">ویرایش</a></td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>

@endsection
