﻿</body>
<!-- jQuery Version 1.11.0 -->
<script src="{{asset('dist/js/jquery-1.11.0.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{asset('dist/js/metisMenu/metisMenu.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{asset('dist/js/raphael/raphael.min.js')}}"></script>
<script src="{{asset('dist/js/morris/morris.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{asset('dist/js/sb-admin-2.js')}}"></script>

<script src="{{asset('dist/js/custom-admin.js')}}"></script>



<script type="text/javascript">

    @if(count($errors)>0)
    @foreach($errors->all() as $error)
        alert({{$error}})
    {{--toastError("{{$error}}",10000,'top-left')--}}
    @endforeach
    @endif



    @if (\Session::has('error'))
    alert({{$error}})
    {{--toastError( "{!! \Session::get('error') !!}",10000,'top-left')--}}
    @endif

    @if (\Session::has('success'))
    alert({{$error}})
    {{--toastSuccess("{!! \Session::get('success') !!}",10000,'top-right')--}}
    @endif

</script>

</html>
