﻿@include('admin.header')

<body>

<div id="wrapper">

   @include('admin.nav')
    <div id="page-wrapper">
        @yield('content')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
@include('admin.footer')
