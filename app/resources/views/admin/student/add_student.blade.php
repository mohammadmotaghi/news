@extends('admin.master')
@section('content')
<form role="form" action="{!! route('store.student') !!}" method="post">
    @csrf
    <div class="form-group has-success">
        <label class="control-label" for="inputSuccess">کد ملی</label>
        <input type="text" class="form-control" name="national_code" id="inputSuccess">
    </div>

    <div class="form-group has-success">
        <label class="control-label" for="inputSuccess">نام </label>
        <input type="text" class="form-control" name="firstname" id="">
    </div>
    <div class="form-group has-success">
        <label class="control-label" for="inputSuccess">نام خانوادگی </label>
        <input type="text" class="form-control" name="lastname" id="">
    </div>
    <div class="form-group">
        <label>مقطع</label>
        <select class="form-control" name="section_id" id="sections">

            <option value="0" selected>-- انتخاب کنید --</option>

            @foreach($sections as $section)
                <option value="{{$section->id}}"> {{$section->title}}</option>
            @endforeach


        </select>
    </div>

    <div class="form-group">
        <label>کلاس</label>
        <select class="form-control" name="class_id" id="classes">
            <option value="0" selected>-- انتخاب کنید --</option>
        </select>
    </div>
    <button type="submit" class="btn btn-success btn-block">ثبت دانش آموز جدید</button>
</form>
@endsection
