@extends('admin.master')
@section('content')
    <div class="row"><div class="col-sm-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 180px;">کد ملی</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 240px;">نام</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 223px;">نام خانوادگی </th>
                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 156px;">کلاس</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 116px;">مقطع تحصیلی</th></tr>
                </thead>
                <tbody>
                @foreach($list as $item)
                <tr class="gradeA odd" role="row">
                    <td class="sorting_1">{{$item->national_code}}</td>
                    <td>{{$item->firstname}}</td>
                    <td>{{$item->lastname}}</td>
                    <td class="center">{{$item->classroom->title}}</td>
                    <td class="center">{{$item->classroom->section->title}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
