<?php

namespace Database\Seeders;

use App\Models\Section\Section;
use Illuminate\Database\Seeder;
use Psy\Util\Str;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       if (Section::count() == 0){
           $types =[
               ['title'=>'پیش دبستانی'],
               ['title'=>'ابتدایی'],
               ['title'=>'دییرستان دوره اول'],
               ['title'=>'دییرستان دوره دوم'],

           ];
           Section::insert($types);
       }

    }
}
