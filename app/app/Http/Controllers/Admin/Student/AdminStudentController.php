<?php namespace App\Http\Controllers\Admin\Student;


use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterStudentRequest;
use App\Services\Admin\ClassAndSectionServices\AdminStudentService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class AdminStudentController extends Controller
{

    public function __construct(AdminStudentService $service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function allStudent()
    {
        return jsonApiResponse(
            200,
            "success",
            $this->service->all()
        );

    }

    public  function  createNewStudent()
    {
        return $this->service->createNewStudent();
    }

    /**
     * @param RegisterStudentRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function storeStudent(RegisterStudentRequest $request)
    {
        return jsonApiResponse(
            200,
            "success",
            $this->service->store($request->validated())
        );
    }


}
