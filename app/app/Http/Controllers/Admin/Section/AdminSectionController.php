<?php namespace App\Http\Controllers\Admin\Section;


use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\GetClassBySectionIdRequest;
use App\Services\Admin\ClassAndSectionServices\AdminSectionService;
use Illuminate\Http\JsonResponse;

class AdminSectionController extends Controller
{

    public function __construct(AdminSectionService  $service)
    {
        $this->service = $service;
    }


    /**
     * @param GetClassBySectionIdRequest $request
     * @return JsonResponse
     */
    public function getClassesSection(GetClassBySectionIdRequest  $request)
    {
        return jsonApiResponse(
            200,
            "ok",
            $this->service->getClassesSection($request->validated())
        );
    }

}
