<?php namespace App\Http\Controllers\Admin\ClassRoom;


use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClassRoomRequest;
use App\Http\Requests\RegisterStudentRequest;
use App\Services\Admin\ClassAndSectionServices\AdminClassRoomService;

class AdminClassRoomController extends Controller
{

    public function __construct(AdminClassRoomService $service)
    {
        $this->service = $service;
    }

    /**
     * list students
     */
    public function  allClassRoom()
    {

        return $this->service->all();
    }

    /**
     *
     */
    public function deleteClassRoom()
    {
        return $this->service->delete();
    }

    /**
     *show form for create new class room
     */
    public function createClassRoom()
    {
        return $this->service->createClassRoom();
    }


    /**
     * @param StoreClassRoomRequest $request
     * @return mixed
     */
    public function storeClassRoom(StoreClassRoomRequest $request)
    {
        return $this->service->storeClassRoom($request->validated());
    }


}
