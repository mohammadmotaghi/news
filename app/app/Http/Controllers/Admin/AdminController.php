<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\AdminService;


class AdminController extends Controller
{
   public  function __construct(AdminService $adminService)
   {
       $this->service = $adminService;
   }

    public  function dashboard()
   {
       return view('Admin.panel');
   }


}
