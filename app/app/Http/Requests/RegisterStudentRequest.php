<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class RegisterStudentRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'national_code'=> 'required|integer',
            'firstname' =>'required|string',
            'lastname'=>'required|string',
            'class_id'=>'required|integer|exists:class_rooms,id',
            'student_info.father_name' => 'required|string',
            'student_info.mother_name' => 'required|string',
        ];
    }
}
