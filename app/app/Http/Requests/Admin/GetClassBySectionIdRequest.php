<?php namespace App\Http\Requests\Admin;


use App\Exceptions\CustomUnAuthorizedException;
use App\Exceptions\CustomValidationException;
use Illuminate\Foundation\Http\FormRequest;

class GetClassBySectionIdRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'section_id'=> 'required|integer',
        ];
    }


    /**
     * @param null $keys
     * @return array
     * @throws CustomUnAuthorizedException
     * @throws CustomValidationException
     */
    public function all( $keys = null)
    {
        $request = request();
        $request['section_id'] = $request->section_id;
        return $request->toArray();
    }
}
