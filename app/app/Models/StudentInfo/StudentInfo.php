<?php

namespace App\Models\StudentInfo;

use App\Models\Student\Student;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StudentInfo extends Model
{
    protected  $table = 'student_infos';

    protected $fillable =[
        //| id          | bigint unsigned | NO   | PRI | NULL    | auto_increment |
         'student_id',  //| bigint unsigned | NO   | MUL | NULL    |                |
         'avatar',      //| varchar(255)    | NO   |     | NULL    |                |
         'father_name', //| varchar(255)    | NO   |     | NULL    |                |
         'mother_name', //| varchar(255)    | NO   |     | NULL    |                |
        //| created_at    | timestamp       | YES  |     | NULL    |                |
        //| updated_at    | timestamp       | YES  |     | NULL    |
    ];


    /**
     * @return BelongsTo
     */
    public function student()
    {
      return  $this->belongsTo(Student::class);
    }
}
