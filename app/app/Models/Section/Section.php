<?php

namespace App\Models\Section;

use App\Models\ClassRoom\ClassRoom;
use App\Models\Student\Student;
use App\Models\StudentInfo\StudentInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'sections';

    public $timestamps = false;

    protected $fillable = [
        //| id         | bigint unsigned | NO   | PRI | NULL    | auto_increment |
           'title'//   | varchar(255)    | NO   | MUL | NULL    |                |
        //| created_at | timestamp       | YES  |     | NULL    |                |
        //| updated_at | timestamp       | YES  |     | NULL    |
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classRooms()
    {
        return $this->hasMany(ClassRoom::class);
    }

}
