<?php

namespace App\Models\Student;

use App\Models\ClassRoom\ClassRoom;
use App\Models\Mobile\Mobile;
use App\Models\Section\Section;
use App\Models\StudentInfo\StudentInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Student extends Model
{
    protected $table = 'students';


    protected $fillable = [
        //      | id            | bigint unsigned | NO   | PRI | NULL    | auto_increment |
        'national_code',  //| int             | NO   | UNI | NULL    |                |
        'firstname',     // | varchar(255)    | NO   |     | NULL    |                |
        'lastname',     // | varchar(255)    | NO   |     | NULL    |                |
        'class_id',      //| bigint unsigned | NO   |     | NULL    |                |
        //| created_at    | timestamp       | YES  |     | NULL    |                |
        //| updated_at    | timestamp       | YES  |     | NULL    |                |

    ];

    protected $hidden = [
        'national_code'
    ];


    protected $with = [
        'studentInfo',
        'mobiles',
        'classroom'
    ];

    protected $appends = [
        'time'
    ];



    public function getFirstnameAttribute($val)
    {
        return strtoupper($val);
    }

    public function getTimeAttribute()
    {
        return time();
    }


   //relations
    /**
     * @return BelongsTo
     */
    public  function  classRoom()
   {
    return $this->belongsTo(ClassRoom::class,'class_id','id');
   }

    /**
     * @return HasMany
     */
    public function mobiles()
    {
      return $this->hasMany(Mobile::class);
    }


    /**
     * @return HasOne
     */
    public function studentInfo()
    {
        return $this->hasOne(StudentInfo::class);
    }

}
