<?php

namespace App\Models\ClassRoom;

use App\Models\Section\Section;
use App\Models\Student\Student;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ClassRoom extends Model
{
    protected  $table = 'class_rooms';
    protected $fillable =[
        //| id         | bigint unsigned | NO   | PRI | NULL    | auto_increment |
          'title',       // | varchar(255)    | NO   | MUL | NULL    |                |
          'section_id'  //| bigint unsigned | NO   |     | NULL    |                |
        //| created_at | timestamp       | YES  |     | NULL    |                |
        //| updated_at | timestamp       | YES  |     | NULL    |
];

    /**
     * @return BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id', 'id');
    }

}
