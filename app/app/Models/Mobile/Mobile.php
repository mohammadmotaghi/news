<?php

namespace App\Models\Mobile;

use App\Models\Student\Student;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    use HasFactory;

    protected $table  = 'mobiles';

    protected $fillable = [
        //| id             | bigint unsigned | NO   | PRI | NULL    | auto_increment |
         'student_id',   //| bigint unsigned | NO   |     | NULL    |                |
         'number_phone', //| varchar(255)    | NO   |     | NULL    |                |
        //| created_at     | timestamp       | YES  |     | NULL    |                |
        //| updated_at     | timestamp       | YES  |     | NULL    |                |
    ];

    /**
     *
     */
    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
