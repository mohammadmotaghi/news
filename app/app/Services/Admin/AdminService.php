<?php


namespace App\Services\Admin;


use App\Repositories\Admin\AddClassRepository;
use App\Services\ServiceBase;

class AdminService extends ServiceBase
{
    public function __construct(AddClassRepository $repository)
    {
      $this->repository = $repository;
    }

    public function registerClassroom(array $params)
    {

        return $create = $this->repository->updateOrCreate(
          ['title'=> $params['title'], 'section_id'=>$params['section_id']],
        );
//        if(! $create){
//          return  'نمی توانید ایجاد کنید ';
//        }

    }
}
