<?php namespace App\Services\Admin\ClassAndSectionServices;


use App\Models\Section\Section;
use App\Repositories\Admin\ClassRoomRepository;
use App\Services\ServiceBase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class AdminClassRoomService extends ServiceBase
{

    public function __construct(ClassRoomRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Application|Factory|View
     */
    public function all()
    {
        $list = $this->repository->all();
        return view('admin.class_room.list_class', compact('list'));
    }

    /**
     * show form page
     * @return Application|Factory|View
     */
    public function createClassRoom()
    {
        $sections  = Section::all();
        return view('admin.class_room.add_class', compact('sections'));
    }

    public function deleteClassRoom($id)
    {
        $list =$this->repository->delete($id);
    }

    /**
     * @param array $params
     */
    public function storeClassRoom(array  $params)
    {
        $store = $this->repository->create($params);
        if(! $store){
            dd('sdsddsdsd');
        }
        return redirect('list_class');
    }



}
