<?php namespace App\Services\Admin\ClassAndSectionServices;


use App\Repositories\SectionRepository;
use App\Services\ServiceBase;

class AdminSectionService extends ServiceBase
{

    public function __construct(SectionRepository  $repository)
    {
        $this->repository = $repository;
    }

    public function getClassesSection(array $params)
    {
        return $this->repository->getClassesSection($params['section_id']);
    }

}
