<?php


namespace App\Services\Admin\ClassAndSectionServices;

use App\Models\ClassRoom\ClassRoom;
use App\Models\Student\Student;
use App\Repositories\Admin\StudentRepository;
use App\Repositories\SectionRepository;
use App\Services\ServiceBase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class AdminStudentService extends ServiceBase
{

  protected  $sectionRepository;


  public function __construct(StudentRepository $repository)
  {
      $this->repository = $repository;
      $this->sectionRepository = app(SectionRepository::class);
  }


    /**
     * @return array
     */
    public  function all() :array
    {
          return $this->repository->all();

    }

    /**
     * @return Application|Factory|View
     */
    public function createNewStudent()
    {
        $sections = $this->sectionRepository->all();
        return view('admin.student.add_student', compact('sections'));
    }
    /**
     * @param array $params
     * @return Application|RedirectResponse|Redirector
     */
    public function store(array $params)
    {

        $store = $this->repository->create($params);
        if(! $store){
            dd('dsdsdsds');
        }
        $store->studentInfo()->create($params['student_info']);
        return $store->toArray();

    }
}
