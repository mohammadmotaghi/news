<?php


namespace App\Repositories\Admin;


use App\Models\ClassRoom\ClassRoom;
use App\Repositories\RepositoriesBase;

class ClassRoomRepository extends RepositoriesBase
{
    public function __construct(ClassRoom $classRoom)
    {
        $this->model = $classRoom;
    }

    public function all()
    {
        return $this->model->with('section')->get();
    }

    /**
     * @param int $id
     * @return bool|null
     * @throws \Exception
     */
    public function deleteClassRoom(int $id)
    {
        return $this->model->delete($id);
    }

    /**
     * return all class of section by section id
     * @param int $id
     * @return ClassRoom
     */
    public function getAllClassOfSectionBySectionId(int $id)
    {
        return $this->model->where('section_id', $id)->get()->toArray();
    }
}
