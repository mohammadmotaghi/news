<?php


namespace App\Repositories\Admin;


use App\Models\Student\Student;
use App\Repositories\RepositoriesBase;

class StudentRepository extends RepositoriesBase
{

    public function __construct(Student $student)
    {
        $this->model = $student;
    }



    public function all() : array
    {
        return $this->model->with(['classRoom' => function($q) {
            $q->with('section');
        }])->get()->makeVisible('national_code')->toArray();
    }
}
