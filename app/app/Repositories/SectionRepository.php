<?php namespace App\Repositories;


use App\Models\ClassRoom\ClassRoom;
use App\Models\Section\Section;
use App\Repositories\Admin\ClassRoomRepository;

class SectionRepository extends RepositoriesBase
{
    protected $classRoomRepository;


    public function __construct(Section $section)
    {
        $this->model = $section;
        $this->classRoomRepository = app(ClassRoomRepository::class);
    }


    /**
     * @param int $id
     * @return ClassRoom
     */
    public function getClassesSection(int $id)
    {
        return $this->classRoomRepository->getAllClassOfSectionBySectionId($id);
    }


}
