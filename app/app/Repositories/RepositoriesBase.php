<?php
namespace App\Repositories;

use phpDocumentor\Reflection\Types\Integer;

class RepositoriesBase
{
   protected $model;


    /**
     * @param array $params
     * @return mixed
     */
    public function create(array $params)
    {
       return $this->model->create($params);
    }

    /**
     * @return mixed
     */
    public function all()
    {
      return $this->model->all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public  function delete(int $id)
    {
        return $this->model->where('id',$id)->destroy();
        //return redirect()->route('dashboard');
    }
}
