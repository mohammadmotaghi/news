<?php


function jsonApiResponse(
    int $status = 0 ,
    string $pm = 'your request successfully handled.!' ,
    array $data = [] ,
    $statusCode = 200 ,
    $refresh = 0 )
{
    return response( [
        'status'  => $status ,
        'pm'      => $pm ,
        'data'    => $data ,
        'refresh' => $refresh ,
    ] , $statusCode );
}


